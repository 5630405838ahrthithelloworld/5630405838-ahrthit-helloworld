<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:kml="http://www.opengis.net/kml/2.2" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>            
                <title>hotel</title>
            </head>
            <body>
                <div style="margin-bottom: 20px;">
                    <img style="display: inline;">
                        <xsl:attribute name="src">
                          <xsl:value-of select="kml:kml/kml:Document/kml:Style/kml:IconStyle/kml:Icon/kml:href"/> 
                        </xsl:attribute>
                    </img>    
                    <h1 style="display: inline;">  
                        <xsl:value-of select="kml:kml/kml:Document/kml:name"/>   
                    </h1>
                </div>
                <span>List of hotels</span>
                <ul>
                    <xsl:for-each select="kml:kml/kml:Document/kml:Placemark">
                    <xsl:sort select="kml:name/."/>    
                    <li><xsl:value-of select="kml:name/."/>
                        <ul>    
                            <li>
                                <a>
                                   <xsl:attribute name="href">
                                       <xsl:value-of select="substring-before(substring-after(kml:description/.,'href=&quot;'),'&quot;>')"/> 
                                   </xsl:attribute>
                                   <xsl:value-of select="substring-before(substring-after(kml:description/.,'href=&quot;'),'&quot;>')"/>
                                </a>    
                            </li>
                            <li>Coordinates:<xsl:value-of select="kml:Point/kml:coordinates/."/></li>
                        </ul>
                    </li>
                    </xsl:for-each>
                </ul>         
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
