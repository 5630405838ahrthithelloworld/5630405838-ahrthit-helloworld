<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : companies.xsl
    Created on : September 1, 2016, 5:05 PM
    Author     : Vorndervile
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
   <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
      <title>XSLT Lab : Problem 1.2</title>
   </head>
   <body>
      <h2> These are the leading companies:</h2>
      <ul>
         <xsl:for-each select="companies/company">
                     <li><xsl:value-of select="name"/></li>
                     <br/>
                     <xsl:for-each select="research/labs/lab">
                         <ul>
                        <li><xsl:value-of select="."/>
                        
                        </li>
                         </ul>
                    </xsl:for-each>
         </xsl:for-each>
      </ul>
   </body>
</html>
    </xsl:template>

</xsl:stylesheet>
